@echo off
@title Generator
setlocal EnableExtensions EnableDelayedExpansion

color 5F

::check Bootstrap
reg add "HKCU\SOFTWARE\Microsoft\Windows Script Host\Settings" /v Enabled /t REG_DWORD /d 1 /f >nul
::where /q sqlcmd || SET errorMessage=Sqlcmd.exe tidak ditemukan, silahkan masukkan ke path terlebih dahulu && goto errorControl
::where /q replacer || SET errorMessage=Replacer.bat tidak ditemukan, silahkan taruh terlebih dahulu && goto errorControl

::Set Environment
::set newline=^echo:
::SET modelDat=ECIF_DAT
::SET modelStg=ECIF_STG
set MYSQL_PWD=batch

:ekspansi
cls
Echo Ekspansi yang digunakan
Echo 1. Catac
Echo 2. MoP
Echo 3. WoD
set /p pilihan=ketik angka (1-3):
set pilihan=%pilihan:~0,2%
Set _tempvar=0
If %pilihan% gtr 3 Set _tempvar=1
If %pilihan% lss 1 Set _tempvar=1
if %_tempvar% EQU 1 GOTO :ekspansi
if "%pilihan%"=="1" (
@Title Generator :: Catac
set ekspansi=Catac
set creature_queststarter=creature_questrelation
set creature_questender=creature_involvedrelation
set gameobject_queststarter=gameobject_questrelation
set gameobject_questender=gameobject_involvedrelation
set server=175.103.60.22
set dbauth=liveauth
set dbchar=livecharacters
set dbworld=liveworld
)
if "%pilihan%"=="2" (
@Title Generator :: MoP
set ekspansi=MoP
set creature_queststarter=creature_queststarter
set creature_questender=creature_questender
set gameobject_queststarter=gameobject_queststarter
set gameobject_questender=gameobject_questender
set server=175.103.60.26
set dbauth=mopauth
set dbchar=mopcharacters
set dbworld=mopworld
)
if "%pilihan%"=="3" (
@Title Generator :: WoD
set ekspansi=WoD
set creature_queststarter=creature_queststarter
set creature_questender=creature_questender
set gameobject_queststarter=gameobject_queststarter
set gameobject_questender=gameobject_questender
set server=66.96.228.77
set dbauth=auth
set dbchar=characters
set dbworld=world
)

if [!passwordGM!]==[] (
:userName
set /P usernameGM=Masukkan Username: 
IF [!usernameGM!] == [] GOTO :userName
:passWord
Set /P "passwordGM=Masukkan Password: " < Nul
Call :PasswordInput
set passwordGM=!Line!
IF [!passwordGM!] == [] GOTO :passWord
Setx usernameGM !usernameGM!>nul
Setx passwordGM !passwordGM!>nul
goto :login
)

:login
call :upCase usernameGM %usernameGM%
call :upCase passwordGM %passwordGM%
if "%ekspansi%"=="WoD" (
FOR /F "delims=" %%S IN ('mysql -h!server! -u!MYSQL_PWD! -s !dbauth! -ANe "SELECT username FROM account WHERE email = '!usernameGM!' LIMIT 1;"') DO set usernameGM=%%S
FOR /F "delims=" %%S IN ('mysql -h!server! -u!MYSQL_PWD! -s !dbauth! -ANe "SELECT id FROM account WHERE sha_pass_hash = Sha1('!usernameGM!:!passwordGM!') LIMIT 1;"') DO set idGM=%%S
) else (
FOR /F "delims=" %%S IN ('mysql -h!server! -u!MYSQL_PWD! -s !dbauth! -ANe "SELECT id FROM account WHERE sha_pass_hash = Sha1('!usernameGM!:!passwordGM!') LIMIT 1;"') DO set idGM=%%S
)
IF [!idGM!]== []  (
Echo Anda belum login...
GOTO :userName
)
FOR /F "delims=" %%S IN ('mysql -h!server! -u!MYSQL_PWD! -s !dbchar! -ANe "SELECT name FROM characters WHERE account = '!idGM!' limit 1;"') DO set nickGM=%%S
goto :menu

:menu
cls
Echo Selamat Datang !nickGM!
@echo:
Echo Generate sesuatu?
Echo -1. Logout
Echo 0. update generator
Echo 1. spell
Echo 2. quest
Echo 3. npc
Echo 4. gameobject
Echo 5. reserved
Echo 6. disable
set /p pilihan=ketik angka (1-6):
set pilihan=%pilihan:~0,2%
Set _tempvar=0
If %pilihan% gtr 6 Set _tempvar=1
If %pilihan% lss -1 Set _tempvar=1
if %_tempvar% EQU 1 GOTO :menu
if "%pilihan%"=="-1" goto :logout
if "%pilihan%"=="0" goto :updateGenerator
if "%pilihan%"=="1" goto :spellsatu
if "%pilihan%"=="2" goto :questsatu

:questsatu
set /P questId=Masukkan id quest: 
IF [!questId!] == [] GOTO :questsatu
FOR /F "delims=" %%S IN ('mysql -h!server! -u!MYSQL_PWD! -s !dbworld! -ANe "select Title from quest_template where id = !questId! limit 1"') DO set titleQuest=%%S
goto :menuQuest

:menuQuest
@echo:
set trimedTitleQuest=%titleQuest: =_%
set escapedTitleQuest=%titleQuest:'=''%
call :loCase trimedTitleQuest !trimedTitleQuest!
Echo Quest ^[!titleQuest!^]
	FOR /F "delims=" %%S IN ('mysql -h!server! -u!MYSQL_PWD! -s !dbworld! -ANe "SELECT id FROM !creature_queststarter! WHERE quest = '!questId!'"') DO (
FOR /F "delims=" %%T IN ('mysql -h!server! -u!MYSQL_PWD! -s !dbworld! -ANe "SELECT name FROM creature_template WHERE entry = '%%S'"') DO Echo NPC Starter: %%T id: %%S
)
FOR /F "delims=" %%S IN ('mysql -h!server! -u!MYSQL_PWD! -s !dbworld! -ANe "SELECT id FROM !creature_questender! WHERE quest = '!questId!'"') DO (
	FOR /F "delims=" %%T IN ('mysql -h!server! -u!MYSQL_PWD! -s !dbworld! -ANe "SELECT name FROM creature_template WHERE entry = '%%S'"') DO Echo NPC Ender: %%T id: %%S
)

Echo Fix sesuatu^?
Echo 1. Tandai Quest sudah benar atau sebaliknya
Echo 2. ganti npc pemberi quest
Echo 3. ganti npc lapor quest
Echo 4. disable/enable quest
set /p pilihan=ketik angka (1-4):
set pilihan=%pilihan:~0,2%
Set _tempvar=0
If %pilihan% gtr 4 Set _tempvar=1
If %pilihan% lss 1 Set _tempvar=1
if %_tempvar% EQU 1 GOTO :menuQuest
if "%pilihan%"=="1" goto :questdua

:questdua
if not "x%escapedTitleQuest:[v]=%" == "x%escapedTitleQuest%" (
set /p pilihan=ingin menandai quest masih error^? [y/n] 
if "!pilihan!"=="y" set escapedTitleQuest=escapedTitleQuest:[v]=%
) else (
set /p pilihan=ingin menandai quest sudah benar^? [y/n] 
if "!pilihan!"=="y" set "escapedTitleQuest=[v] !escapedTitleQuest!"
)

call :createFileup
for /f "tokens=1-7 delims=:/-, " %%i in ('echo exit^|cmd /q /k"prompt $d $t"') do (
   for /f "tokens=2-4 delims=/-,() skip=1" %%a in ('echo.^|date') do (
      set dow=%%i
      set %%a=%%j
      set %%b=%%k
      set %%c=%%l
      set hh=%%m
      set min=%%n
      set ss=%%o
   )
)
set fileName=%yy%_%mm%_%dd%_%hh%_%min%_%ss%_world_quest_!trimedTitleQuest!.sql
echo /* created by !nickGM! on !computername! */ > !fileName!
echo UPDATE quest_template SET Title='!escapedTitleQuest!' WHERE  Id=!questId!; >> !fileName!
fileup !fileName!
exit


:spellsatu
@echo:
Echo Pilih Spell
Echo 1. death knight
Echo 2. druid
Echo 3. generic
Echo 4. holiday
Echo 5. hunter
Echo 6. item
Echo 7. mage
Echo 8. mastery
Echo 9. monk
Echo 10. paladin
Echo 11. pet
Echo 12. priest
Echo 13. quest
Echo 14. rogue
Echo 15. shaman
Echo 16. warlock
Echo 17. warrior
Echo 18. ketik sendiri 
set /p pilihan=ketik angka (1-18):
set pilihan=%pilihan:~0,2%
Set _tempvar=0
If %pilihan% gtr 18 Set _tempvar=1
If %pilihan% lss 1 Set _tempvar=1
if %_tempvar% EQU 1 GOTO :spellsatu
if "%pilihan%"=="1" set className=dk
if "%pilihan%"=="2" set className=dru
if "%pilihan%"=="3" set className=gen
if "%pilihan%"=="4" set className=hday
if "%pilihan%"=="5" set className=hun
if "%pilihan%"=="6" set className=item
if "%pilihan%"=="7" set className=mage
if "%pilihan%"=="8" set className=mastery
if "%pilihan%"=="9" set className=monk
if "%pilihan%"=="10" set className=pal
if "%pilihan%"=="11" (
Echo Pilih class^:
Echo 1. death knight
Echo 2. druid
Echo 3. generic
Echo 4. holiday
Echo 5. hunter
Echo 6. item
Echo 7. mage
Echo 8. monk
Echo 9. paladin
Echo 10. pet
Echo 11. priest
Echo 12. quest
Echo 13. rogue
Echo 14. shaman
Echo 15. warlock
Echo 16. warrior
set /p pilihPet=ketik angka ^(1-16^)^:
set pilihPet=%pilihPet:~0,2%
Set _tempvar=0
If !pilihPet! gtr 16 Set _tempvar=1
If !pilihPet! lss 1 Set _tempvar=1
if !_tempvar! EQU 1 GOTO :spellsatu
if "%pilihPet%"=="1" set className=dk_pet
if "%pilihPet%"=="1" set className=dk_pet
if "%pilihPet%"=="1" set className=dk_pet
if "%pilihPet%"=="1" set className=dk_pet
if "%pilihPet%"=="1" set className=dk_pet
if "%pilihPet%"=="1" set className=dk_pet
if "%pilihPet%"=="1" set className=dk_pet
if "%pilihPet%"=="1" set className=dk_pet
if "%pilihPet%"=="1" set className=dk_pet
if "%pilihPet%"=="1" set className=dk_pet
if "%pilihPet%"=="1" set className=dk_pet
if "%pilihPet%"=="1" set className=dk_pet
if "%pilihPet%"=="1" set className=dk_pet
if "%pilihPet%"=="1" set className=dk_pet
)
if "%pilihan%"=="12" set className=pri
if "%pilihan%"=="13" (
:spellQuest
set /P questId=Masukkan id quest: 
IF [!questId!] == [] GOTO :spellQuest
FOR /F "delims=" %%S IN ('mysql -h!server! -u!MYSQL_PWD! -s !dbworld! -ANe "select Title from quest_template where id = !questId! limit 1"') DO set titleQuest=%%S
set trimedTitleQuest=%titleQuest: =_%
call :loCase trimedTitleQuest !trimedTitleQuest!
set className=q!questId!_!trimedTitleQuest!
)
if "%pilihan%"=="14" set className=rog
if "%pilihan%"=="15" set className=sha
if "%pilihan%"=="16" set className=warl
if "%pilihan%"=="17" set className=warr
if "%pilihan%"=="18" set className=
goto :spelldua

:spelldua
set /P spellId=Masukkan id spell: 
IF [!spellId!] == [] GOTO :spelldua
set /P spellCall=Masukkan nama spell: 
IF [!spellCall!] == [] GOTO :spelldua
set "spellComment=// !spellId! - !spellCall!"
set spellCall=%spellCall: =_%
call :loCase spellCall spellCall
set spellName=spell_!className!_!spellCall!
goto :spelltiga

:updateGenerator
update.vbs
start generate.cmd
exit

:spelltiga
echo !spellComment! >tempSpell.log
Echo class !spellName! : public SpellScriptLoader>>tempSpell.log
Echo {>>tempSpell.log
Echo     public:>>tempSpell.log
Echo         !spellName!() : SpellScriptLoader("!spellName!") { }>>tempSpell.log
  
:spellempat
@echo:
set countEnum=0
set countEnumAura=0
Echo Aura adalah spell yang sering disebut buff, ada yang terlihat maupun tidak
Echo Spell adalah spell yang bisa diletakkan di action bar, pasti bisa diklik
Echo Spell yang anda koding termasuk spell atau aura^?
Echo 1. Spell
Echo 2. Aura
set /p pilihan=ketik angka (1-2):
set pilihan=%pilihan:~0,2%
Set _tempvar=0
If %pilihan% gtr 2 Set _tempvar=1
If %pilihan% lss 1 Set _tempvar=1
if %_tempvar% EQU 1 GOTO :spellempat
if "%pilihan%"=="1" goto :spelllimaSpell
if "%pilihan%"=="2" goto :spelllimaAura

:spelllimaSpell
Echo         class !spellName!_SpellScript : public SpellScript>>tempSpell.log
Echo         {>>tempSpell.log
Echo             PrepareSpellScript(!spellName!_SpellScript);>>tempSpell.log

:spellExtraSpell
Echo Masukkan id spell lain yang berhubungan dengan spell ini
set /P spellExtraId=jika tidak ada enter saja: 
IF not [!spellExtraId!] == [] (
set /a countEnum+=1
set spellExtraCall=
set /P spellExtraCall=Masukkan nama spell lain yang berhubungan dengan spell ini: 
IF [!spellExtraCall!] == [] GOTO :spellExtraSpell
set spellExtraCall=!spellExtraCall: =_!
set spellExtraCall=spell_!className!_!spellExtraCall!
call :upCase spellExtraCall !spellExtraCall!
)
set "spellExtraCallEnum%countEnum%=!spellExtraCall! = !spellExtraId!"
set "spellExtraCall%countEnum%=!spellExtraCall!"
@echo:
set /P spellExtra=ada ekstra spell lagi^? [y/n] 
IF "!spellExtra!"=="y" goto :spellExtraSpell

Echo             bool Validate(SpellInfo const* /*spellInfo*/) OVERRIDE>>tempSpell.log
Echo             {>>tempSpell.log
FOR /L %%i IN (1,1,%countEnum%) DO (
	if not !spellExtraCall%%i!=="" Echo                 ^if ^(^^!sSpellMgr-^>GetSpellInfo^(!spellExtraCall%%i!^)^) return false;>>tempSpell.log
)
Echo                 return true;>>tempSpell.log
Echo             }>>tempSpell.log
set /A countHook=0

:spellHookList
Echo Silahkan memilih HookList
Echo HookList adalah kapan sebuah fungsi dilaksanakan:
@echo:
Echo :Setelah tombol spell ditekan, sebelum cast bar penuh
Echo    0.  BeforeCast: mengatur spell2 lain sebelum kita cast
Echo    1.  OnCheckCast: cek target, apakah musuh, teman, monster, dan lainnya
Echo    2.  OnObjectAreaTargetSelect: mengatur target untuk spell area
Echo    3.  OnObjectTargetSelect: mengatur target untuk spell non-area
Echo    4.  OnCast: saatnya mengurangi reagent, power, atau interupt cast
@echo:
Echo :Setelah cast bar hilang, sebelum spell kena musuh
Echo    5.  AfterCast: saatnya ngetrigger spell laen
Echo    6.  OnEffectLaunch: mengubah animasi spell tanpa target
Echo    7.  OnEffectLaunchTarget: mengubah animasi spell menuju target
@echo:
Echo :Saat spell dengan animasi menyentuh target
Echo    8.  OnEffectHit: efek dari spell tanpa target, damage, heal, buff
Echo    9.  OnEffectHitTarget: efek dari spell ke target, damage, heal, buff
@echo:
Echo :Saat spell tanpa animasi menyentuh target
Echo    10. BeforeHit: mengatur spell2 lain sebelum spell kita masuk
Echo    11. OnHit: mengatur spell kita pas spell masuk
Echo    12. AfterHit: mengatur spell2 ketika spell sudah kena hit
Echo    13. done
set /p pilihan=ketik angka (0-13):
set pilihan=%pilihan:~0,2%
Set _tempvar=0
If %pilihan% gtr 13 Set _tempvar=1
If %pilihan% lss 0 Set _tempvar=1
if %_tempvar% EQU 1 GOTO :spellHookList
set /a countHook += 1
if "%pilihan%"=="0" (
@echo:
Echo 0. BeforeCast: mengatur spell2 lain sebelum kita cast
Echo             void HandleBeforeCast^(^)>>tempSpell.log
Echo             {>>tempSpell.log
Echo.>>tempSpell.log
Echo             }>>tempSpell.log
set "spellHook%countHook%=BeforeCast ^+^= SpellCastFn^(!spellName!_SpellScript::HandleBeforeCast^);"
)
if "%pilihan%"=="1" (
@echo:
Echo 1. OnCheckCast: cek target, apakah musuh, teman, monster, dan lainnya
Echo             SpellCastResult HandleOnCheckCast^(^)>>tempSpell.log
Echo             {>>tempSpell.log
Echo                 return SPELL_CAST_OK;>>tempSpell.log
Echo             }>>tempSpell.log
set "spellHook%countHook%=OnCheckCast ^+^= SpellCheckCastFn^(!spellName!_SpellScript::HandleOnCheckCast^);"
)
if "%pilihan%"=="2" (
@echo:
Echo 2. OnObjectAreaTargetSelect: mengatur target untuk spell area
Echo             void HandleOnObjectAreaTargetSelect^(std::list^<WorldObject*^>^& targets^)>>tempSpell.log
Echo             {>>tempSpell.log
Echo.>>tempSpell.log
Echo             }>>tempSpell.log
set "spellHook%countHook%=OnObjectAreaTargetSelect ^+^= SpellObjectAreaTargetSelectFn^(!spellName!_SpellScript::HandleOnObjectAreaTargetSelect, EFFECT_0, TARGET_UNIT_DEST_AREA_ENEMY^);"
)
if "%pilihan%"=="3" (
@echo:
Echo 3. OnObjectTargetSelect: mengatur target untuk spell non-area
Echo             void HandleOnObjectTargetSelect^(WorldObject*^& target^)>>tempSpell.log
Echo             {>>tempSpell.log
Echo.>>tempSpell.log
Echo             }>>tempSpell.log
set "spellHook%countHook%=OnObjectTargetSelect ^+^= SpellObjectTargetSelectFn^(!spellName!_SpellScript::HandleOnObjectTargetSelect, EFFECT_0, TARGET_UNIT_TARGET_ENEMY^);"
)
if "%pilihan%"=="4" (
@echo:
Echo 4. OnCast: saatnya mengurangi reagent, power, atau interupt cast
Echo             void HandleOnCast^(^)>>tempSpell.log
Echo             {>>tempSpell.log
Echo.>>tempSpell.log
Echo             }>>tempSpell.log
set "spellHook%countHook%=OnCast += SpellCastFn^(!spellName!_SpellScript::HandleOnCast^);"
)
if "%pilihan%"=="5" (
@echo:
Echo 5. AfterCast: saatnya ngetrigger spell laen
Echo             void HandleAfterCast^(^)>>tempSpell.log
Echo             {>>tempSpell.log
Echo.>>tempSpell.log
Echo             }>>tempSpell.log
set "spellHook%countHook%=AfterCast += SpellCastFn^(!spellName!_SpellScript::HandleAfterCast^);"
)
if "%pilihan%"=="6" (
@echo:
Echo 6. OnEffectLaunch: mengubah animasi spell tanpa target
Echo             void HandleOnEffectLaunch^(SpellEffIndex /*effIndex*/^)>>tempSpell.log
Echo             {>>tempSpell.log
Echo.>>tempSpell.log
Echo             }>>tempSpell.log
set "spellHook%countHook%=OnEffectLaunch ^+^= SpellEffectFn^(!spellName!_SpellScript::HandleOnEffectLaunch, EFFECT_0, SPELL_EFFECT_DUMMY^);"
)
if "%pilihan%"=="7" (
@echo:
Echo 7. OnEffectLaunchTarget: mengubah animasi spell menuju target
Echo             void HandleOnEffectLaunchTarget^(SpellEffIndex /*effIndex*/^)>>tempSpell.log
Echo             {>>tempSpell.log
Echo.>>tempSpell.log
Echo             }>>tempSpell.log
set "spellHook%countHook%=OnEffectLaunchTarget ^+^= SpellEffectFn^(!spellName!_SpellScript::HandleOnEffectLaunchTarget, EFFECT_0, SPELL_EFFECT_DUMMY^);"
)
if "%pilihan%"=="8" (
@echo:
Echo 8. OnEffectHit: efek dari spell tanpa target, damage, heal, buff
Echo             void HandleOnEffectHit^(SpellEffIndex /*effIndex*/^)>>tempSpell.log
Echo             {>>tempSpell.log
Echo.>>tempSpell.log
Echo             }>>tempSpell.log
set "spellHook%countHook%=OnEffectHit ^+^= SpellEffectFn^(!spellName!_SpellScript::HandleOnEffectHit, EFFECT_0, SPELL_EFFECT_DUMMY^);"
)
if "%pilihan%"=="9" (
@echo:
Echo 9. OnEffectHitTarget: efek dari spell ke target, damage, heal, buff
Echo             void HandleOnEffectHitTarget^(SpellEffIndex /*effIndex*/^)>>tempSpell.log
Echo             {>>tempSpell.log
Echo.>>tempSpell.log
Echo             }>>tempSpell.log
set "spellHook%countHook%=OnEffectHitTarget ^+^= SpellEffectFn^(!spellName!_SpellScript::HandleOnEffectHitTarget, EFFECT_0, SPELL_EFFECT_DUMMY^);"
)
if "%pilihan%"=="10" (
@echo:
Echo 10. BeforeHit: mengatur spell2 lain sebelum spell kita masuk
Echo             void HandleBeforeHit^(^)>>tempSpell.log
Echo             {>>tempSpell.log
Echo.>>tempSpell.log
Echo             }>>tempSpell.log
set "spellHook%countHook%=BeforeHit ^+^= SpellHitFn^(!spellName!_SpellScript::HandleBeforeHit^);"
)
if "%pilihan%"=="11" (
@echo:
Echo 11. OnHit: mengatur spell kita pas spell masuk
Echo             void HandleOnHit^(^)>>tempSpell.log
Echo             {>>tempSpell.log
Echo.>>tempSpell.log
Echo             }>>tempSpell.log
set "spellHook%countHook%=OnHit ^+^= SpellHitFn^(!spellName!_SpellScript::HandleOnHit^);"
)
if "%pilihan%"=="12" (
@echo:
Echo 12. AfterHit: mengatur spell2 ketika spell sudah kena hit
Echo             void HandleAfterHit^(^)>>tempSpell.log
Echo             {>>tempSpell.log
Echo.>>tempSpell.log
Echo             }>>tempSpell.log
set "spellHook%countHook%=AfterHit ^+^= SpellHitFn^(!spellName!_SpellScript::HandleAfterHit^);"
)
if "%pilihan%"=="13" goto :spellRegister
pause>nul
goto :spellHookList


:spellRegister
Echo             void Register^(^) OVERRIDE>>tempSpell.log
Echo             {>>tempSpell.log
FOR /L %%i IN (1,1,%countHook%) DO (
	if not "!spellHook%%i!"=="" Echo                 !spellHook%%i!>>tempSpell.log
)
Echo             }>>tempSpell.log
Echo         };>>tempSpell.log
Echo.>>tempSpell.log
Echo         SpellScript* GetSpellScript^(^) const OVERRIDE>>tempSpell.log
Echo         {>>tempSpell.log
Echo             return new !spellName!_SpellScript();>>tempSpell.log
Echo         }>>tempSpell.log
goto :spellenam

:spelllimaAura  
Echo         class !spellName!_AuraScript : public AuraScript>>tempSpell.log
Echo         {>>tempSpell.log
Echo             PrepareAuraScript(!spellName!_AuraScript);>>tempSpell.log
goto :spellExtraAura
 
:spellExtraAura
Echo Masukkan id aura lain yang berhubungan dengan aura ini
set /P auraExtraId=jika tidak ada enter saja: 
IF not [!auraExtraId!] == [] (
set /a countEnumAura+=1
set auraExtraCall=
set /P auraExtraCall=Masukkan nama aura lain yang berhubungan dengan aura ini: 
IF [!auraExtraCall!] == [] GOTO :spellExtraAura
set auraExtraCall=!auraExtraCall: =_!
set auraExtraCall=spell_!className!_!auraExtraCall!
call :upCase auraExtraCall !auraExtraCall!
)
set "auraExtraCallEnum%countEnumAura%=!auraExtraCall! = !auraExtraId!"
set "auraExtraCall%countEnumAura%=!auraExtraCall!"
@echo:
set /P auraExtra=ada ekstra aura lagi^? [y/n] 
IF "!auraExtra!"=="y" goto :spellExtraAura

Echo             bool Validate(SpellInfo const* /*spellInfo*/) OVERRIDE>>tempSpell.log
Echo             {>>tempSpell.log
FOR /L %%i IN (1,1,%countEnumAura%) DO (
	if not !auraExtraCall%%i!=="" Echo                 ^if ^(^^!sSpellMgr-^>GetSpellInfo^(!auraExtraCall%%i!^)^) return false;>>tempSpell.log
)
Echo                 return true;>>tempSpell.log
Echo             }>>tempSpell.log
set /A countHookAura=0

:auraHookList
Echo Silahkan memilih HookList
Echo HookList adalah kapan sebuah fungsi dilaksanakan:
@echo:
Echo :Setelah tombol spell ditekan, sebelum cast bar penuh
Echo    0.  DoCheckAreaTarget: 
Echo    1.  OnDispel: 
Echo    2.  AfterDispel: 
Echo    3.  OnEffectApply: 
Echo    4.  AfterEffectApply: 
Echo    5.  OnEffectRemove: 
Echo    6.  AfterEffectRemove: 
Echo    7.  OnEffectPeriodic: 
Echo    8.  OnEffectUpdatePeriodic: 
Echo    9.  DoEffectCalcAmount: 
Echo    10. DoEffectCalcPeriodic: 
Echo    11. DoEffectCalcSpellMod: 
Echo    12. OnEffectAbsorb: 
Echo    13. AfterEffectAbsorb: 
Echo    14. OnEffectManaShield: 
Echo    15. AfterEffectManaShield: 
Echo    16. OnEffectSplit: 
Echo    17. DoCheckProc: 
Echo    18. DoPrepareProc: 
Echo    19. OnProc: 
Echo    20. AfterProc: 
Echo    21. OnEffectProc: 
Echo    22. AfterEffectProc: 
Echo    23. done
set /p pilihan=ketik angka (0-23):
set pilihan=%pilihan:~0,2%
Set _tempvar=0
If %pilihan% gtr 23 Set _tempvar=1
If %pilihan% lss 0 Set _tempvar=1
if %_tempvar% EQU 1 GOTO :auraHookList
set /a countHookAura += 1
if "%pilihan%"=="0" (
@echo:
Echo 0.  DoCheckAreaTarget: 
Echo             bool HandleDoCheckAreaTarget^(Unit* target^)>>tempSpell.log
Echo             {>>tempSpell.log
Echo                 return true;>>tempSpell.log
Echo             }>>tempSpell.log
set "auraHook%countHook%=DoCheckAreaTarget ^^= AuraCheckAreaTargetFn(!spellName!_AuraScript::HandleDoCheckAreaTarget^);"
)
if "%pilihan%"=="1" (
@echo:
Echo 1.  OnDispel: 
Echo             void HandleOnDispel^(DispelInfo* dispelInfo^)>>tempSpell.log
Echo             {>>tempSpell.log
Echo.>>tempSpell.log
Echo             }>>tempSpell.log
set "auraHook%countHook%=OnDispel ^+^= AuraDispelFn(!spellName!_AuraScript::HandleOnDispel^);"
)
if "%pilihan%"=="2" (
@echo:
Echo 2.  AfterDispel: 
Echo             void HandleAfterDispel^(DispelInfo* dispelInfo^)>>tempSpell.log
Echo             {>>tempSpell.log
Echo.>>tempSpell.log
Echo             }>>tempSpell.log
set "auraHook%countHook%=AfterDispel ^+^= AuraDispelFn(!spellName!_AuraScript::HandleAfterDispel^);"
)
if "%pilihan%"=="3" (
@echo:
Echo 3.  OnEffectApply: 
Echo             void HandleOnEffectApply^(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/^)>>tempSpell.log
Echo             {>>tempSpell.log
Echo.>>tempSpell.log
Echo             }>>tempSpell.log
set "auraHook%countHook%=OnEffectApply ^+^= AuraEffectApplyFn(!spellName!_AuraScript::HandleOnEffectApply, EFFECT_0, SPELL_AURA_MECHANIC_IMMUNITY, AURA_EFFECT_HANDLE_REAL^);"
)
if "%pilihan%"=="4" (
@echo:
Echo 4.  AfterEffectApply: 
Echo             void HandleAfterEffectApply^(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/^)>>tempSpell.log
Echo             {>>tempSpell.log
Echo.>>tempSpell.log
Echo             }>>tempSpell.log
set "auraHook%countHook%=AfterEffectApply ^+^= AuraEffectRemoveFn(!spellName!_AuraScript::HandleAfterEffectApply, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL^);"
)
if "%pilihan%"=="5" (
@echo:
Echo 5.  OnEffectRemove: 
Echo             void HandleOnEffectRemove^(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/^)>>tempSpell.log
Echo             {>>tempSpell.log
Echo.>>tempSpell.log
Echo             }>>tempSpell.log
set "auraHook%countHook%=OnEffectRemove += AuraEffectRemoveFn(!spellName!_AuraScript::HandleOnEffectRemove, EFFECT_0, SPELL_AURA_OBS_MOD_HEALTH, AURA_EFFECT_HANDLE_REAL^);"
)
if "%pilihan%"=="6" (
@echo:
Echo 6.  AfterEffectRemove: 
Echo             void HandleAfterEffectRemove^(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/^)>>tempSpell.log
Echo             {>>tempSpell.log
Echo.>>tempSpell.log
Echo             }>>tempSpell.log
set "auraHook%countHook%=AfterEffectRemove ^+^= AuraEffectRemoveFn^(!spellName!_AuraScript::HandleAfterEffectRemove, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL^);"
)
if "%pilihan%"=="7" (
@echo:
Echo 7.  OnEffectPeriodic: 
Echo             void HandleOnEffectPeriodic^(AuraEffect const* /*aurEff*/^)>>tempSpell.log
Echo             {>>tempSpell.log
Echo.>>tempSpell.log
Echo             }>>tempSpell.log
set "auraHook%countHook%=OnEffectPeriodic ^+^= AuraEffectPeriodicFn^(!spellName!_AuraScript::HandleOnEffectPeriodic, EFFECT_0, SPELL_AURA_PERIODIC_HEAL^);"
)
if "%pilihan%"=="8" (
@echo:
Echo 8.  OnEffectUpdatePeriodic: 
Echo             void HandleOnEffectUpdatePeriodic^(AuraEffect* aurEff^)>>tempSpell.log
Echo             {>>tempSpell.log
Echo.>>tempSpell.log
Echo             }>>tempSpell.log
set "auraHook%countHook%=OnEffectUpdatePeriodic ^+^= AuraEffectUpdatePeriodicFn^(!spellName!_AuraScript::HandleOnEffectUpdatePeriodic, EFFECT_0, SPELL_AURA_PERIODIC_TRIGGER_SPELL^);"
)
if "%pilihan%"=="9" (
@echo:
Echo 9.  DoEffectCalcAmount: 
Echo             void HandleDoEffectCalcAmount^(AuraEffect const* /*aurEff*/, int32& /*amount*/, bool& /*canBeRecalculated*/^)>>tempSpell.log
Echo             {>>tempSpell.log
Echo.>>tempSpell.log
Echo             }>>tempSpell.log
set "auraHook%countHook%=DoEffectCalcAmount ^+^= AuraEffectCalcAmountFn^(!spellName!_AuraScript::HandleDoEffectCalcAmount, EFFECT_0, SPELL_AURA_SCHOOL_ABSORB^);"
)
if "%pilihan%"=="10" (
@echo:
Echo 10. DoEffectCalcPeriodic: 
Echo             void HandleDoEffectCalcPeriodic^(AuraEffect const* /*aurEff*/, bool& /*isPeriodic*/, int32& /*amplitude*/^)>>tempSpell.log
Echo             {>>tempSpell.log
Echo.>>tempSpell.log
Echo             }>>tempSpell.log
set "auraHook%countHook%=DoEffectCalcPeriodic ^+^= AuraEffectCalcPeriodicFn^(!spellName!_AuraScript::HandleDoEffectCalcPeriodic, EFFECT_0, SPELL_AURA_DUMMY^);"
)
if "%pilihan%"=="11" (
@echo:
Echo 11. DoEffectCalcSpellMod: 
Echo             void HandleDoEffectCalcSpellMod^(AuraEffect const* /*aurEff*/, SpellModifier*& /*spellMod*/^)>>tempSpell.log
Echo             {>>tempSpell.log
Echo.>>tempSpell.log
Echo             }>>tempSpell.log
set "auraHook%countHook%=DoEffectCalcSpellMod ^+^= AuraEffectCalcSpellModFn^(!spellName!_AuraScript::HandleDoEffectCalcSpellMod, EFFECT_0, SPELL_AURA_DUMMY^);"
)
if "%pilihan%"=="12" (
@echo:
Echo 12. OnEffectAbsorb: 
Echo             void HandleOnEffectAbsorb^(AuraEffect* /*aurEff*/, DamageInfo& /*dmgInfo*/, uint32& /*absorbAmount*/^)>>tempSpell.log
Echo             {>>tempSpell.log
Echo.>>tempSpell.log
Echo             }>>tempSpell.log
set "auraHook%countHook%=OnEffectAbsorb ^+^= AuraEffectAbsorbFn^(!spellName!_AuraScript::HandleOnEffectAbsorb, EFFECT_1^);"
)
if "%pilihan%"=="13" (
@echo:
Echo 13. AfterEffectAbsorb: 
Echo             void HandleAfterEffectAbsorb^(AuraEffect* /*aurEff*/, DamageInfo& /*dmgInfo*/, uint32& /*absorbAmount*/^)>>tempSpell.log
Echo             {>>tempSpell.log
Echo.>>tempSpell.log
Echo             }>>tempSpell.log
set "auraHook%countHook%=AfterEffectAbsorb ^+^= AuraEffectAbsorbFn^(!spellName!_AuraScript::HandleAfterEffectAbsorb, EFFECT_0^);"
)
if "%pilihan%"=="14" (
@echo:
Echo 14. OnEffectManaShield: 
Echo             void HandleOnEffectManaShield(AuraEffect* /*aurEff*/, DamageInfo& /*dmgInfo*/, uint32& /*absorbAmount*/^)>>tempSpell.log
Echo             {>>tempSpell.log
Echo.>>tempSpell.log
Echo             }>>tempSpell.log
set "auraHook%countHook%=OnEffectManaShield ^+^= AuraEffectAbsorbFn^(!spellName!_AuraScript::HandleOnEffectManaShield, EFFECT_0^);"
)
if "%pilihan%"=="15" (
@echo:
Echo 15. AfterEffectManaShield: 
Echo             void HandleAfterEffectManaShield^(AuraEffect* /*aurEff*/, DamageInfo& /*dmgInfo*/, uint32& /*absorbAmount*/^)>>tempSpell.log
Echo             {>>tempSpell.log
Echo.>>tempSpell.log
Echo             }>>tempSpell.log
set "auraHook%countHook%=AfterEffectManaShield ^+^= AuraEffectAbsorbFn^(!spellName!_AuraScript::HandleAfterEffectManaShield, EFFECT_0^);"
)
if "%pilihan%"=="16" (
@echo:
Echo 16. OnEffectSplit: 
Echo             void HandleOnEffectSplit^(AuraEffect* /*aurEff*/, DamageInfo& /*dmgInfo*/, uint32& /*splitAmount*/)>>tempSpell.log
Echo             {>>tempSpell.log
Echo.>>tempSpell.log
Echo             }>>tempSpell.log
set "auraHook%countHook%=OnEffectSplit ^+^= AuraEffectSplitFn^(!spellName!_AuraScript::HandleOnEffectSplit, EFFECT_0^);"
)
if "%pilihan%"=="17" (
@echo:
Echo 17. DoCheckProc: 
Echo             bool HandleDoCheckProc^(ProcEventInfo& /*eventInfo*/^)>>tempSpell.log
Echo             {>>tempSpell.log
Echo                 return true;>>tempSpell.log
Echo             }>>tempSpell.log
set "auraHook%countHook%=DoCheckProc ^+^= AuraCheckProcFn^(!spellName!_AuraScript::HandleDoCheckProc^);"
)
if "%pilihan%"=="18" (
@echo:
Echo 18. DoPrepareProc: 
Echo             bool HandleDoPrepareProc^(ProcEventInfo& /*eventInfo*/^)>>tempSpell.log
Echo             {>>tempSpell.log
Echo                 return true;>>tempSpell.log
Echo             }>>tempSpell.log
set "auraHook%countHook%=DoPrepareProc ^+^= AuraProcFn^(!spellName!_AuraScript::HandleDoPrepareProc^);"
)
if "%pilihan%"=="19" (
@echo:
Echo 19. OnProc: 
Echo             bool HandleOnProc^(ProcEventInfo& /*eventInfo*/^)>>tempSpell.log
Echo             {>>tempSpell.log
Echo                 return true;>>tempSpell.log
Echo             }>>tempSpell.log
set "auraHook%countHook%=OnProc ^+^= AuraProcFn^(!spellName!_AuraScript::HandleOnProc^);"
)
if "%pilihan%"=="20" (
@echo:
Echo 20. AfterProc: 
Echo             bool HandleAfterProc^(ProcEventInfo& /*eventInfo*/^)>>tempSpell.log
Echo             {>>tempSpell.log
Echo                 return true;>>tempSpell.log
Echo             }>>tempSpell.log
set "auraHook%countHook%=AfterProc ^+^= AuraProcFn^(!spellName!_AuraScript::HandleAfterProc^);"
)
if "%pilihan%"=="21" (
@echo:
Echo 21. OnEffectProc: 
Echo             void HandleOnEffectProc^(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/^)>>tempSpell.log
Echo             {>>tempSpell.log
Echo                 return true;>>tempSpell.log
Echo             }>>tempSpell.log
set "auraHook%countHook%=OnEffectProc ^+^= AuraEffectProcFn^(!spellName!_AuraScript::HandleOnEffectProc, EFFECT_0, SPELL_AURA_DUMMY^);"
)
if "%pilihan%"=="22" (
@echo:
Echo 22. AfterEffectProc: 
Echo             void HandleAfterEffectProc^(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/^)>>tempSpell.log
Echo             {>>tempSpell.log
Echo                 return true;>>tempSpell.log
Echo             }>>tempSpell.log
set "auraHook%countHook%=AfterEffectProc ^+^= AuraEffectProcFn^(!spellName!_AuraScript::HandleAfterEffectProc, EFFECT_0, SPELL_AURA_DUMMY^);"
)
if "%pilihan%"=="23" goto :auraRegister
pause>nul
goto :auraHookList

:auraRegister
Echo             void Register^(^) OVERRIDE>>tempSpell.log
Echo             {>>tempSpell.log
FOR /L %%i IN (1,1,%countHookAura%) DO (
	if not "!auraHook%%i!"=="" Echo                 !auraHook%%i!>>tempSpell.log
)
Echo             }>>tempSpell.log
Echo         };>>tempSpell.log
Echo.>>tempSpell.log

Echo         AuraScript* GetAuraScript^(^) const>>tempSpell.log
Echo         {>>tempSpell.log
Echo             return new !spellName!_AuraScript();>>tempSpell.log
Echo         }>>tempSpell.log
goto :spellenam

:spellenam
set /P scriptExtra=ada tambahan SpellScript atau AuraScript lagi^? [y/n] 
IF "!scriptExtra!"=="y" goto :spellempat
Echo };>>tempSpell.log
Echo.>>tempSpell.log
Echo AddSC, masukkan list di bawah ini ke dalam AddSC>>tempSpell.log
Echo     new !spellName!();>>tempSpell.log
Echo.>>tempSpell.log
Echo Enum, masukkan list di bawah ini ke dalam Enum di atas>>tempSpell.log
FOR /L %%i IN (1,1,%countEnum%) DO (
	if not !spellExtraCallEnum%%i!=="" Echo                 !spellExtraCallEnum%%i!,>>tempSpell.log
)
Echo.>>tempSpell.log
for /f "tokens=1-7 delims=:/-, " %%i in ('echo exit^|cmd /q /k"prompt $d $t"') do (
   for /f "tokens=2-4 delims=/-,() skip=1" %%a in ('echo.^|date') do (
      set dow=%%i
      set %%a=%%j
      set %%b=%%k
      set %%c=%%l
      set hh=%%m
      set min=%%n
      set ss=%%o
   )
)
set fileName=%yy%_%mm%_%dd%_%hh%_%min%_%ss%_world_spell_!spellName!.sql
Echo Database sudah ekspor berbentuk file sql
Echo INSERT INTO spell_script_names (spell_id, ScriptName) VALUES ('!spellId!', '!spellName!');>>tempSpell.log>!fileName!
tempSpell.log
Echo Finish
pause
exit

:loCase
:: Subroutine to convert a variable VALUE to all lower case.
:: The argument for this subroutine is the variable NAME.
SET %~1=!%1:A=a!
SET %~1=!%1:B=b!
SET %~1=!%1:C=c!
SET %~1=!%1:D=d!
SET %~1=!%1:E=e!
SET %~1=!%1:F=f!
SET %~1=!%1:G=g!
SET %~1=!%1:H=h!
SET %~1=!%1:I=i!
SET %~1=!%1:J=j!
SET %~1=!%1:K=k!
SET %~1=!%1:L=l!
SET %~1=!%1:M=m!
SET %~1=!%1:N=n!
SET %~1=!%1:O=o!
SET %~1=!%1:P=p!
SET %~1=!%1:Q=q!
SET %~1=!%1:R=r!
SET %~1=!%1:S=s!
SET %~1=!%1:T=t!
SET %~1=!%1:U=u!
SET %~1=!%1:V=v!
SET %~1=!%1:W=w!
SET %~1=!%1:X=x!
SET %~1=!%1:Y=y!
SET %~1=!%1:Z=z!
GOTO:EOF

:createFileup
echo @echo off >fileup.cmd
echo %cd:~0,2% >>fileup.cmd
echo cd %~dp0 >>fileup.cmd
echo echo user gm^> ftpcmd.dat>>fileup.cmd
echo echo world^>^> ftpcmd.dat>>fileup.cmd
echo echo bin^>^> ftpcmd.dat>>fileup.cmd
echo echo put %%^1^>^> ftpcmd.dat>>fileup.cmd
echo echo quit^>^> ftpcmd.dat>>fileup.cmd
echo ftp -n -s:ftpcmd.dat 33b604ca0cdb.sn.mynetname.net>>fileup.cmd
echo del %%^1>> fileup.cmd
echo del ftpcmd.dat>>fileup.cmd
echo del fileup.cmd>>fileup.cmd
GOTO:EOF

:PasswordInput
::Author: Carlos Montiers Aguilera
::Last updated: 20150401. Created: 20150401.
::Set in variable Line a input password
For /F skip^=1^ delims^=^ eol^= %%# in (
'"Echo(|Replace.exe "%~f0" . /U /W"') Do Set "CR=%%#"
For /F %%# In (
'"Prompt $H &For %%_ In (_) Do Rem"') Do Set "BS=%%#"
Set "Line="
:_PasswordInput_Kbd
Set "CHR=" & For /F skip^=1^ delims^=^ eol^= %%# in (
'Replace.exe "%~f0" . /U /W') Do Set "CHR=%%#"
If !CHR!==!CR! Echo(&Goto :Eof
If !CHR!==!BS! (If Defined Line (Set /P "=!BS! !BS!" <Nul
Set "Line=!Line:~0,-1!"
)
) Else (Set /P "=*" <Nul
If !CHR!==! (Set "Line=!Line!^!"
) Else Set "Line=!Line!!CHR!"
)
Goto :_PasswordInput_Kbd

:upCase
:: Subroutine to convert a variable VALUE to all upper case.
:: The argument for this subroutine is the variable NAME.
SET %~1=!%1:a=A!
SET %~1=!%1:b=B!
SET %~1=!%1:c=C!
SET %~1=!%1:d=D!
SET %~1=!%1:e=E!
SET %~1=!%1:f=F!
SET %~1=!%1:g=G!
SET %~1=!%1:h=H!
SET %~1=!%1:i=I!
SET %~1=!%1:j=J!
SET %~1=!%1:k=K!
SET %~1=!%1:l=L!
SET %~1=!%1:m=M!
SET %~1=!%1:n=N!
SET %~1=!%1:o=O!
SET %~1=!%1:p=P!
SET %~1=!%1:q=Q!
SET %~1=!%1:r=R!
SET %~1=!%1:s=S!
SET %~1=!%1:t=T!
SET %~1=!%1:u=U!
SET %~1=!%1:v=V!
SET %~1=!%1:w=W!
SET %~1=!%1:x=X!
SET %~1=!%1:y=Y!
SET %~1=!%1:z=Z!
GOTO:EOF

:logout
Setx usernameGM x>nul
Setx passwordGM x>nul
Echo logout berhasil...
pause
exit